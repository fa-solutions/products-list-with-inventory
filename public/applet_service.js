let FAClient = null;
let recordId = null;
let linesGlobal = null;
let productList = null;

// aHR0cDovL2xvY2FsaG9zdDo1MDAw localhost:5000
//aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3Byb2R1Y3RzLWxpc3QtbWFzdGVyLw

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cDovL2xvY2FsaG9zdDo1MDAw`,
};

let config = {
     lines: {
         name: '',
         fields: {}
     },
     products: {
         name: '',
         fields: {}
     },
     parent : {
         name: ''
     },
     helpText: {
         key: '',
         value: null
     },
     style: {
         input: {
             width : null
         },
         text: {
             mainTextSize: null,
             helpTextSize: null
         }
     }
}


function startupService() {
    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    const {
        productsApp= 'product',
        parentApp= 'quote',
        linesApp= 'line_item',
    } = FAClient.params;

    const {
        productName = `${productsApp}_field0`,
        productPrice = `${productsApp}_field1`,
        productStock = `${productsApp}_field7`,
        helpText= 'In Stock',
        helpTextField= null,
        productImg = `${productsApp}_field3`,
        productDescription= null,
        lineName = `${linesApp}_field0`,
        lineQty = `${linesApp}_field0`,
        inputWidth= null,
        mainTextSize=null,
        helpTextSize=null
    } = FAClient.params;

    config.products.name = productsApp;
    config.parent.name = parentApp;
    config.lines.name = linesApp;

    config.helpText.key = helpText;
    config.helpText.valueField = helpTextField;

    config.style.input.width = inputWidth;

    config.style.text = {
        mainTextSize,
        helpTextSize
    }

    config.products.fields = {
        name: productName,
        price: productPrice,
        img: productImg,
        stock: productStock,
        description: productDescription
    };

    config.lines.fields = {
        name: lineName,
        qty: lineQty
    };

    FAClient.on("openProducts", (data) => {
        console.log(data);
        recordId = data.id;
        FAClient.listEntityValues(
            {
                entity: config.lines.name,
                filters: [
                    {
                        field_name: "parent_entity_reference_id",
                        operator: "includes",
                        values: [data.id],
                    },
                ],
            },
            (lines) => {
                console.log(lines);
                linesGlobal = lines;
                FAClient.open();
                FAClient.listEntityValues({entity: config.products.name}, (data) => {
                    productList = data;
                    generateList({products: data, lines})
                })
            });
    });

    let searchEl = document.getElementById('search');
    searchEl.addEventListener("keyup", (event) => {
        let value = event.target.value.toLowerCase();
        if (productList) {
            let filteredList = productList.filter(prod => prod.field_values[config.products.fields.name].display_value.toLowerCase().includes(value))
            generateList({ products:filteredList, lines: linesGlobal })
            console.log(filteredList)
        }
    });

}

function stripHtml(html)
{
    let tmp = document.createElement("DIV");
    tmp.innerHTML = html;
    return tmp.textContent || tmp.innerText || "";
}

function generateList({products, lines}) {
    let list = document.getElementById('order-list');
    console.log(list);
    list.innerHTML = '';
    console.log(config.lines.fields);
    products.map(product => {
        let lineMatch = lines.find(line => line.field_values[config.lines.fields.name].value === product.id);
        let qty = 0;
        if (lineMatch) {
            qty = lineMatch.field_values[config.lines.fields.qty].display_value;
        }
        let fieldValues = product.field_values;
        let priceField = fieldValues[config.products.fields.price];
        let priceFormated = priceField.formatted_value;
        let productName = fieldValues[config.products.fields.name].display_value;
        let productImg = fieldValues[config.products.fields.img].display_value;
        let productStock = fieldValues[config.products.fields.stock].value;


        let liEl = document.createElement('li');
        let thumbnailContainer = document.createElement('div');
        let thumbnail = document.createElement('img');
        thumbnailContainer.appendChild(thumbnail);
        thumbnailContainer.setAttribute("class", "img-container");
        thumbnail.setAttribute("src", productImg);
        let heather4 = document.createElement('h4');
        let heather4Text = document.createTextNode(`${productName} ${priceFormated}`);
        if (config.style.text.mainTextSize && config.style.text.mainTextSize.includes('px')) {
            heather4.style.fontSize = config.style.text.mainTextSize;
        }
        heather4.appendChild(heather4Text);
        let heather5 = null;
        if (config.helpText.valueField && fieldValues[config.helpText.valueField]) {
            let displayHelperText = '';
            if (fieldValues[config.helpText.valueField].formatted_value) {
                displayHelperText = 0;
            } else {
                displayHelperText =  stripHtml(fieldValues[config.helpText.valueField].display_value)
            }
            heather5 = document.createElement('h5');
            let heather5Text = document.createTextNode(`${config.helpText.key}: ${displayHelperText}`);
            if (config.style.text.helpTextSize && config.style.text.helpTextSize.includes('px')) {
                heather5.style.fontSize = config.style.text.helpTextSize;
            }
            heather5.appendChild(heather5Text);
        }

        let input = document.createElement('input');
        input.setAttribute("type", "number");
        input.setAttribute("min", "0");
        input.setAttribute("value", `${qty}`);
        input.setAttribute("data-prev", `${qty}`)
        input.setAttribute("name", fieldValues[config.products.fields.name].value);
        if (config.style.input.width) {
            input.style.width = config.style.input.width;
        }

        input.addEventListener("change", (event) => {
            console.log(product.id);
            console.log(product.seq_id);
            console.log(productName);
            console.log(event);
            let qty = event.target.valueAsNumber;
            let prevValue = event.target.dataset && event.target.dataset.prev ? parseInt(event.target.dataset.prev) : 0;

            let difference = qty - prevValue;
            let newStock = productStock - difference;
            console.log({difference, productStock, newStock, qty})
            console.log(linesGlobal);
            let lineFound = linesGlobal.find(line => line.field_values[config.lines.fields.name].value === product.id);
            if (lineFound) {
                if (qty > 0) {
                    console.log('it is running')
                    updateQty({id: lineFound.id, qty});
                } else {
                    deleteEntity(lineFound.id)
                    linesGlobal = linesGlobal.filter(line => line.field_values[config.lines.fields.name].value !== product.id);
                    lineFound = null;
                }
            } else {
                if (qty > 0) {
                    addItem({product, qty});
                }
            }
            updateInventory(product.id, newStock);
            event.target.dataset.prev = qty;
            updateRecord();
        })
        let inputContainer = document.createElement('div');
        inputContainer.setAttribute("class", "input-container")
        inputContainer.appendChild(input);
        liEl.appendChild(thumbnailContainer);
        liEl.appendChild(heather4);
        if (heather5) {
            liEl.appendChild(heather5);
        }
        liEl.appendChild(inputContainer);
        list.appendChild(liEl);
    });
}

function deleteEntity(id) {
    let updatePayload = {
        entity: config.lines.name,
        id: id,
        field_values: { deleted: true }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
        updateRecord();
    });
}


function updateQty({id, qty}) {
    let updatePayload = {
        entity: config.lines.name,
        id: id,
        field_values: { [config.lines.fields.qty]: qty }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
        updateRecord();
    })
    console.log(id, qty);
}

function addItem({product, qty}) {
    let payload = {
        entity: config.lines.name,
        field_values: {
            [config.lines.fields.name] : product.id,
            [config.lines.fields.qty]: qty,
            parent_entity_reference_id: ""
        }
    };
    payload.field_values.parent_entity_reference_id = recordId;
    console.log(payload);
    FAClient.createEntity(payload, (data) => {
        console.log(data);
        updateRecord();
        linesGlobal.push(data.entity_value)
    })
}

function updateRecord(id=recordId) {
    let updatePayload = {
        entity: config.parent.name,
        id: id,
        field_values: {}
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}

function updateInventory(id=recordId, newQty) {
    let updatePayload = {
        entity: config.products.name,
        id: id,
        field_values: {
            product_field7 : newQty
        }
    }
    FAClient.updateEntity(updatePayload, (data) => {
        console.log(data);
    })
}
